                 Le dormeur du val 

1:C'est un trou de verdure où chante une rivière,
2:Accrochant follement aux herbes des haillons
3:D'argent ; où le soleil, de la montagne fière,
4:Luit : c'est un petit val qui mousse de rayons. 

5:Un soldat jeune, bouche ouverte, tête nue,
6:Et la nuque baignant dans le frais cresson bleu,
7:Dort ; il est étendu dans l'herbe, sous la nue,
8:Pâle dans son lit vert où la lumière pleut. 

9:Les pieds dans les glaïeuls, il dort. Souriant comme
10:Sourirait un enfant malade, il fait un somme :
11:Nature, berce-le chaudement : il a froid. 

12:Les parfums ne font pas frissonner sa narine ;
13:Il dort dans le soleil, la main sur sa poitrine,
14:Tranquille. Il a deux trous rouges au côté droit. 

           Arthur Rimbaud
